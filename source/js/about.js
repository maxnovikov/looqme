//Counters
const about = document.querySelectorAll('.about__circle-block-title'),
      summaryBlocks = document.querySelectorAll('.about__circle-block-count_text'),
      timeChange = 60,
      finalVals = [2000, 10000, 10];

about.forEach((e, index) => {
      let addVal = Math.ceil(finalVals[index]/timeChange);

   setInterval(() => {
      let counterVal = parseInt(e.innerHTML);
      counterVal = counterVal + 1;
      if (counterVal < finalVals[index]) e.innerHTML = counterVal;
      else {
         e.innerHTML = 0;
         let summaryBlockVal = parseInt(summaryBlocks[index].innerHTML.trim());
         summaryBlockVal = summaryBlockVal + finalVals[index];
         summaryBlocks[index].innerHTML = summaryBlockVal;
      }
   }, 1000/addVal);
});

//Type text

if($('.pr-test__animated-input-text').length) {
    let typedNames = $('.data-invisible .names').text().split(',');

    let typed = new Typed('.pr-test__animated-input-text', {
        strings: typedNames,
        typeSpeed: 100,
        backSpeed: 60,
        loop: true,
        showCursor: false
    });

    document.querySelector('.pr-test__input').addEventListener('focusout', (e) => {
        if(e.target.value === '') {
            setTimeout(function () {
                document.querySelector('.typed-cursor').style.opacity = '';
                typed.start();
            }, 600);
        } else {
            setTimeout(function () {
                e.target.value = null;
                document.querySelector('.typed-cursor').style.opacity = '';
                typed.start();
            }, 20000);
        }
    });
    document.querySelector('.pr-test__input').addEventListener('focusin', (e) => {
        typed.reset();
        typed.stop();
        document.querySelector('.typed-cursor').style.opacity = '0';
    });
}


