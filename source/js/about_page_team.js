//Because of fullPage library principle of work this script was changed.
//In the quotes you can see value of variables that can be used if you off fullPage scroll effect

const teamImgWrapp = document.querySelectorAll('.team__person-img-wrapper'),
      slidesInImg = 30;

teamImgWrapp.forEach((el) => {
    let widthVal = parseInt(el.clientWidth),
        widthValDivide2 = parseInt(widthVal/2);
    el.style.height = widthVal + 'px';

    if($('body').width() < 768) {
        el.style.width = widthVal + 'px';

    }

    window.addEventListener('resize', () => {
        widthVal = el.clientWidth;
        el.style.transition = 'all .01s ease .01s';
        el.style.height = widthVal + 'px';

        if($('body').width() < 768) {
            el.style.width = widthVal + 'px';
        }

        setTimeout(() => {
            el.style.transition = '';
        },100);
    });

    if($('body').width() > 767) {
        el.addEventListener('mousemove', (e) => {
            e.stopPropagation();
            let elemImg = el.getElementsByTagName('img'),
                topPosRelativeBody = el.getBoundingClientRect().top ; //el.offsetTop - el.scrollTop + el.clientTop

            let elemTopCenter = topPosRelativeBody + (widthVal/2),
                elemLeftCenter = el.getBoundingClientRect().left + (widthVal/2);

            let x = 100 - parseInt(Math.abs((e.pageX - elemLeftCenter) / ('.' + widthValDivide2))),
                y = 100 - parseInt((Math.abs(e.pageY - elemTopCenter) / ('.' + widthValDivide2))),
                sqrX = x*x,
                sqrY = y*y,
                modulePos = parseInt(Math.sqrt(sqrX + sqrY));

            if(modulePos > 129) modulePos = 129;

            let productNum = (100 - modulePos) % slidesInImg;
            productNum = productNum  * parseInt(elemImg[0].clientWidth / slidesInImg);


            if(productNum > 0) {
                productNum = 0;
            }

            elemImg[0].style.transform = 'matrix(1, 0, 0, 1, ' + productNum + ', 0)';
        });
    }
});

if($('body').width() < 768 && teamImgWrapp.length) {
    $('.team__person-img-wrapper img').pressure({
        change: function(force, event){
            let value  = (Math.floor(force * 30) * ($(this).width()/30)) - $(this).width()/30;
            $(this).css('transform', 'matrix(1,0,0,1,-' + value + ', 0)');
        }
    }, {only:'touch'});
    // takes 5 seconds to go from a force value of 0 to 1
    // only on devices that do not support pressure
}

if($('body').width() < 768) {
    const personWrapp = document.querySelectorAll('.team__slider-wrapper .team__person-wrapper'),
        personColumn = document.querySelectorAll('.team__column:not(.team__column_first)');
    personWrapp.forEach((el) => {
        document.querySelector('.team__slider-wrapper').appendChild(el);
    });
    personColumn.forEach((el) => {
        el.remove();
    })
}

$('.team__slider-wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    swipe: false,
    appendArrows: $('.team__slider-nav-wrapper'),
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                infinite: false
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                appendArrows: $('.team__slider-wrapper'),
                infinite: false,
                centerMode: true
            }
        },
    ]
});