const faqLink = document.querySelectorAll('.faq .tab-pane__link'),
      faqNavItem = document.querySelectorAll('.faq .tab-pane__nav-item');

faqLink.forEach((element) => {
    element.addEventListener('click', (e) => {
       e.preventDefault();

        let faqLinkAct = document.querySelector('.faq .tab-pane__link.active'),
            faqTabAct = document.querySelector('.faq .tab-pane__tab.active'),
            newTabAct = document.querySelector('.faq .tab-pane__tab[data-anchor="' + element.getAttribute('href') + '"]');

       faqLinkAct.classList.remove('active');
       faqTabAct.classList.remove('active');
       element.classList.add('active');
       document.querySelector('.tab-pane__nav-item.activeLink').classList.remove('activeLink');
       element.parentElement.classList.add('activeLink');

       setTimeout(() => {
           newTabAct.classList.add('active');
       },500);
    });
});

if($('body').width() < 768) {
    faqNavItem.forEach((e) => {
       e.addEventListener('click', function() {
           faqNavItem.forEach((el) => {
              el.classList.toggle('visible');
           });
       });
    });
}

if ($('body').width() > 767) {

    for(let i = 1; i<= Math.ceil(faqNavItem.length/6); i++) {
        let column = document.createElement('div');
        column.classList.add('tab-pane__column');
        column.classList.add('tab-pane__column-' + i);
        document.querySelector('.tab-pane__nav').appendChild(column);
    }

    faqNavItem.forEach((e, i) => {
       document.querySelector('.tab-pane__column-' + Math.floor((6+i)/6)).appendChild(e);
    });

    if(document.querySelector('.tab-pane__tab.active')) {
        document.querySelector('.tab-pane__tab.active').addEventListener('mouseover', function() {
            fullpage_api.setAllowScrolling(false);
        });

        document.querySelector('.tab-pane__tab.active').addEventListener('mouseout', function() {
            fullpage_api.setAllowScrolling(true);
        });
    }
}