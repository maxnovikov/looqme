let selectedClass;

$(".blogs__filters-link").click(function() {
    $('.blogs__filters-link.active').removeClass('active');
    $(this).addClass('active');
    selectedClass = $(this).attr("data-rel");
    $(".blogs__cards").fadeTo(600, 0.1);
    $(".blogs__cards .blogs__card").not("."+selectedClass).fadeOut().removeClass('scale-anm');

    setTimeout(function() {
        if($('.blogs__card.' + selectedClass).length < 1) {
            $('.blogs__cards').addClass('empty-cards');
            $(".empty").fadeIn().addClass('scale-anm');
            $(".blogs__cards").fadeTo(600, 1);
            setColors('.scale-anm');
        } else {
            $('.blogs__cards').removeClass('empty-cards');
            $("."+selectedClass).fadeIn().addClass('scale-anm');
            $(".blogs__cards").fadeTo(600, 1);
            setColors('.scale-anm');
        }
    }, 600);

});

$(document).ready(() => {
    $('.blogs__filters-link.active').click();
});

function setColors(className) {
    $('.blogs__card').removeClass('color-1')
        .removeClass('color-2')
        .removeClass('color-3')
        .removeClass('color-4')
        .removeClass('color-5')
        .removeClass('color-6')
        .removeClass('color-7')
        .removeClass('color-8')
        .removeClass('color-9');

    document.querySelectorAll(className).forEach((el,i) => {
        let num = 1 + i%9;
        el.classList.add('color-' + num);
    });
}