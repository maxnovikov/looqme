let howBlock = document.querySelectorAll('.how__info-block'),
    heightArray = [];

howBlock.forEach((el) => {
    heightArray.push(parseInt(el.clientHeight));
});

const maxHeight = Math.max.apply(null, heightArray);

howBlock.forEach((el) => {
   el.style.height = maxHeight + 'px';
});