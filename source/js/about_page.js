    let worthBlocks = document.querySelectorAll('.worth__block'),
        advantagesBlocks = document.querySelectorAll('.advantages__block'),
        worthBlocksHeights = [],
        largestHeight;

function blocksHeight(blockClass) {
    if (blockClass.length !== 0) {

        blockClass.forEach((el) => {
            let elHeight = el.clientHeight;
            worthBlocksHeights.push(elHeight);
        });

        largestHeight = Math.max.apply(null, worthBlocksHeights);

        blockClass.forEach((el) => {
            el.style.height = largestHeight + 'px';
        });
    }
}

if($('body').width() > 991) {
    blocksHeight(worthBlocks);
    blocksHeight(advantagesBlocks);
}