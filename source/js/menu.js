window.onload = function() {
    const body = document.querySelector('body');
    const burgerBtn = document.getElementById('burger');
    const heroButtonTry = document.querySelectorAll('.hero__button-block-try');
    const navbarButtonTry = document.querySelector('.header__button-block-try');
    const navigationLink = document.querySelectorAll('.c-nav__menu-link');

    navigationLink.forEach((el, i) => {
        el.addEventListener('mouseenter', (e) => {
            e.stopPropagation();
            let newIndex = i + 1;

            if(document.querySelectorAll('.c-nav__menu-tabs .c-nav__menu-tab.visible').length) {
                document.querySelector('.c-nav__menu-tabs .c-nav__menu-tab.visible').classList.remove('visible');
            }
            if(document.querySelectorAll('.c-nav__menu-tabs .c-nav__menu-tab:nth-child(' + newIndex + ')').length) {
                document.querySelector('.c-nav__menu-tabs .c-nav__menu-tab:nth-child(' + newIndex + ')').classList.add('visible');
            }
        })
    });

    burgerBtn.addEventListener("click", (e) => {
        e.preventDefault();
        body.classList.toggle('c-nav-animated');
    });

    window.addEventListener('scroll', () => {
        if(heroButtonTry.length) {
            let buttonPosition = heroButtonTry[0].getBoundingClientRect().top;

            if(buttonPosition < 70) {
                navbarButtonTry.style.opacity = '1';
                heroButtonTry[0].style.opacity = '0';
            } else {
                navbarButtonTry.style.opacity = '0';
                heroButtonTry[0].style.opacity = '1';
            }
        }
    });
}
