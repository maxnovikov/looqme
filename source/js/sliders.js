const clientsSlider = document.querySelectorAll('.clients__slides-wrapper');

if(clientsSlider.length) {
    let countOfClients;

    if($('body').width() > 991)  countOfClients = 3;
    else  countOfClients = 1;

    $('.clients__slides-wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: countOfClients,
        vertical: true,
        infinite: true,
        swipe: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    vertical: false,
                    slidesToShow: 1
                }
            },
        ]
    });
}

$('.clients__slides-wrapper').on('beforeChange', () => {
    const clientSlides = document.querySelectorAll('.slick-slide>div');

    clientSlides.forEach((el, i) => {
        const num = i % 3 +  2;
        el.style.transitionDelay = '.'+ num + 's';
    });
});

$('.clients__logo-slider').slick({
    variableWidth: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 600,
    arrows: false,
    dots: false,
    infinite: true,
    swipe: false,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        },
    ]
});

$('.analytic-section__slide-wrapper').slick({
    vertical: true,
});

if ($('body').width() < 992) {
    $('.js-md-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        adaptiveHeight: true
    });
}

if($('body').width() > 767) {
    $('.faq__wrapper .tab-pane__nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        vertical: true,
        infinite: false,
        adaptiveHeight: true,
    });
}

$('.js-pop-up-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    arrows: true,
    dots: true
});