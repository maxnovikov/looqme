window.addEventListener('mousemove', (e) => {
    let mouseTop = e.clientY,
        mouseLeft = e.clientX;

    let advantages = document.querySelectorAll('.for-whom__advantage'),
        advantagesPosition = [],
        forWhomCircle = document.querySelector('.for-whom__circle');

    advantages.forEach((el) => {
        let rect = el.getBoundingClientRect(),
            elInnerWidth = el.clientWidth,
            elInnerHeight = el.clientHeight;

        let element = {
            id: el.getAttribute('id'),
            top: rect.top,
            left: rect.left,
            right: rect.left + elInnerWidth,
            bottom: rect.top + elInnerHeight
        }
        advantagesPosition.push(element);
    });

    advantagesPosition.forEach((obj) => {
        if(obj.top <= mouseTop && mouseTop <= obj.bottom && obj.left <= mouseLeft && mouseLeft <=obj.right) {
            document.getElementById('' + obj.id).classList.add('active');
            $('.for-whom__advantage').not('#' + obj.id).removeClass('active');

            forWhomCircle.classList = 'for-whom__circle';
            forWhomCircle.classList.add('active-' + obj.id);
        }
    });
});