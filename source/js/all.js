document.addEventListener('DOMContentLoaded', function() {
   document.body.classList.add('loaded');
});

$('.pop-up-button').click(function(e) {
   e.preventDefault();

   let popupTarget = $(this).attr('href');
   $('body').addClass('pop-up-open');

   $(popupTarget).addClass('open');

   document.querySelector('.pop-up.open .pop-up__close').addEventListener('click', (e) => {
       e.preventDefault();
       $(popupTarget).removeClass('open');

       setTimeout(() => {
           $('body').removeClass('pop-up-open');
       },500);
   });
});
