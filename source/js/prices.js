if($('body').hasClass('prices')) {
    let detailsButton = document.querySelectorAll('.services-section__block-details>a'),
        servicesBlock = document.querySelectorAll('.services-section__block');

    detailsButton.forEach( (el) => {
        el.addEventListener('click', (e) => {
            e.preventDefault();
            console.log(e);

            e.path[3].classList.toggle('services-section__block_active');

            if(document.querySelectorAll('.services-section__block_active').length)
                document.querySelector('.prices-section').classList.add('open-service');
            else document.querySelector('.prices-section').classList.remove('open-service');
        });
    });

    servicesBlock.forEach((el) => {
        el.addEventListener('mouseover', function() {
            if(el.classList.contains('services-section__block_active'))
                fullpage_api.setAllowScrolling(false);
        });

        el.addEventListener('mouseleave', function() {
            fullpage_api.setAllowScrolling(true);
        });
    });

    document.addEventListener('DOMContentLoaded', (e) => {
        if(e.target.location.hash) {
            let hash = e.target.location.hash;
            hash = hash.replace('#', '');
            if(document.querySelector('.services-section__block[data-id="'+ hash +'"]')) {
                document.querySelector('.services-section__block[data-id="'+ hash +'"]')
                    .classList.add('services-section__block_active');
                document.querySelector('.prices-section').classList.add('open-service');
            }
        }
    });
}