if( $('body').width() > 767 && $('.js-section-scroll').length) {
    new fullpage('.js-section-scroll', {
        //options here
        responsiveSlides: true,
        fixedElements: '.services__left-side',
        sectionSelector: 'section',
        verticalCentered: false,
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        onLeave: function(index, nextIndex, direction) {
            if (document.querySelector('.services')) {
                let alt = nextIndex.item.attributes[1].nodeValue,
                    dataPopUp = nextIndex.item.attributes[2].nodeValue;

                document.querySelector('.services__left-side-subtitle strong').innerHTML = alt;
                document.querySelector('.services__left-side-digest').attributes[0].value = '#' + dataPopUp;
            }
        }

    });
    fullpage_api.setRecordHistory(true);
}

if($('body').width() < 768) {
    $(document).scroll(function(e) {
        if($(document).scrollTop() > 40) {
            document.querySelector('.header').classList.add('header-scrolled');
        } else {
            document.querySelector('.header').classList.remove('header-scrolled');
        }
    });
}